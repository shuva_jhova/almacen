<?php
global $idorden;
$borde=0;
$alto=2;
$tama�oletra=18;
$id=$_GET['idemp'];
$app;
$datos=array();
$archivos=array();
require("../recursos/FPDFF/fpdf.php");
$interlineado=4;

require_once("appControlalmacen.php");

require_once('../phpqrcode/qrlib.php'); 
require_once('../phpqrcode/qrconfig.php'); 

$app=new appControlalmacen();
date_default_timezone_set('America/Mexico_City');
$var=date('d/m/y')." ".date('g:i:s a');
$datos=$app->getspecificetiquetaingresomp($id);
//trigger_error(print_r($datos,true));
$clavesae=$datos[0]['codigosae'];
while(strlen($clavesae)<8){
$clavesae="0".$clavesae;
}

$nombreqr=$datos[0]['codigosae'].'-'.$datos[0]['id'];

 $tempDir = "../qrcodes/"; 
     
  $codeContents = $datos[0]['id'].'|'.$datos[0]['idconcepto'].'|'.$datos[0]['fechacompleta'].'|'.$datos[0]['concepto'].'|'.$datos[0]['codigosae'].'|'.$datos[0]['producto'].'|'.$datos[0]['almacen'].'|'.$datos[0]['proveedor'].'|'.$datos[0]['cantidad'].'|'.$datos[0]['um'].'|'.$datos[0]['pesoneto'].'|'.$datos[0]['pesobruto'].'|'.$datos[0]['tarima1'].'|'.$datos[0]['tarima2'].'|'.$datos[0]['documento'].'|'.$datos[0]['ubicacion'].'|'.$datos[0]['confirmado'].'|'.$datos[0]['activo'];
     
    
    $fileName = $nombreqr.'.png'; 
     
    $pngAbsoluteFilePath = $tempDir.$fileName; 
    $urlRelativeFilePath = "../qrcodes/".$fileName; 
     
 
    if (file_exists($pngAbsoluteFilePath)) { 
	unlink($pngAbsoluteFilePath);
        QRcode::png($codeContents, $pngAbsoluteFilePath); 
       
    } else { 
         QRcode::png($codeContents, $pngAbsoluteFilePath); 
    } 


class PDF extends FPDF
{

var $widths;
var $aligns;

function SetWidths($w)
{
//Set the array of column widths
$this->widths=$w;
}

function SetAligns($a)
{
//Set the array of column alignments
$this->aligns=$a;
}

function Row($data,$border,$fill='D',$nb=0)
{
    //Calculate the height of the row
    if($nb==0){
    for($i=0;$i<count($data);$i++)
        $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
    $h=5*$nb;
     }else{
	 $h=5*$nb;
	}
    //Issue a page break first if needed
    $this->CheckPageBreak($h);
    //Draw the cells of the row
    for($i=0;$i<count($data);$i++)
    {
        $w=$this->widths[$i];
        $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
        //Save the current position
        $x=$this->GetX();
        $y=$this->GetY();
        //Draw the border
	if($border==1){
        	$this->Rect($x,$y,$w,$h,$fill);
	}
        //Print the text
        $this->MultiCell($w,5,$data[$i],0,$a);
        //Put the position to the right of the cell
        $this->SetXY($x+$w,$y);
    }
    //Go to the next line
    $this->Ln($h);
}

function CheckPageBreak($h)
{
//If the height h would cause an overflow, add a new page immediately
if($this->GetY()+$h>$this->PageBreakTrigger)
$this->AddPage($this->CurOrientation);
}

function NbLines($w,$txt)
{
//Computes the number of lines a MultiCell of width w will take
$cw=&$this->CurrentFont['cw'];
if($w==0)
$w=$this->w-$this->rMargin-$this->x;
$wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
$s=str_replace("\r",'',$txt);
$nb=strlen($s);
if($nb>0 and $s[$nb-1]=="\n")
$nb--;
$sep=-1;
$i=0;
$j=0;
$l=0;
$nl=1;
while($i<$nb)
{
$c=$s[$i];
if($c=="\n")
{
$i++;
$sep=-1;
$j=$i;
$l=0;
$nl++;
continue;
}
if($c==' ')
$sep=$i;
$l+=$cw[$c];
if($l>$wmax)
{
if($sep==-1)
{
if($i==$j)
$i++;
}
else
$i=$sep+1;
$sep=-1;
$j=$i;
$l=0;
$nl++;
}
else
$i++;
}
return $nl;
}
function Footer()
{
$this->SetY(-15);
// Select Arial italic 8
$this->SetFont('Arial','I',12);
$this->SetFont('Arial','I',6);
$this->Cell(0,10,'P�gina '.$this->PageNo().' de {nb}',0,0,'C');



}

function Header()
{
// Logo

$this->SetFont('Arial','',10);
$this->SetXY(49,10);
$this->Cell(56,5,'Nombre del Documento',1,0,'C');
$this->SetXY(105,10);
$this->Cell(90,5,'Etiqueta',1,0,'C');
$this->Image('encabezado.png',12,10,110);
$this->SetXY(49,15);
$this->Cell(20,5,'Revisi�n',1,0,'C');
$this->SetXY(69,15);
$this->Cell(23,5,'Fecha',1,0,'C');
$this->SetXY(92,15);
$this->Cell(41,5,'Elabor�',1,0,'C');
$this->SetXY(133,15);
$this->Cell(40,5,'Autoriz�',1,0,'C');
$this->SetXY(173,15);
$this->Cell(22,5,'C�digo',1,0,'C');
$this->SetXY(49,20);
$this->Cell(20,5,'01',1,0,'C');
$this->SetXY(69,20);
$this->Cell(23,5,'03.Julio.2018',1,0,'C');
$this->SetXY(92,20);
$this->Cell(41,5,'Alfonso Ochoa Maga�a',1,0,'C');
$this->SetXY(133,20);
$this->Cell(40,5,'Jes�s P�rez Miranda',1,0,'C');
$this->SetXY(173,20);
$this->Cell(22,5,'R-ING-01',1,0,'C');
$this->Line(15, 30, 195, 30);
$this->Ln(15);

}
}



$pdf=new PDF();
//$pdf=new PDF_MC_Table();
$pdf->AliasNbPages();
$pdf->PageNo();
$pdf->SetAuthor('JVL');
$pdf->setMargins(15,0,5);
$pdf->AddPage('P');
$pdf->SetTitle("PEMSA");
$pdf->SetAutoPageBreak(true,15);
$pdf->SetTopMargin(0);


$pdf->SetWidths(array(260));
$pdf->SetFont('Arial','B',$tama�oletra+4);
$pdf->SetWidths(array(180));
$pdf->SetAligns(array('C'));
$pdf->Row(array('Etiqueta de Entrada de Materia Prima'),0,'');
$pdf->Ln($interlineado+3);
$pdf->SetFont('Arial','',$tama�oletra);
$pdf->SetWidths(array(105,30,45));
$pdf->SetAligns(array('C','L','L'));
$pdf->Row(array('','Folio:',iconv('UTF-8', 'windows-1252',$datos[0]['id'])),0,'');
$pdf->Ln($interlineado);
$pdf->Row(array('','Fecha:',iconv('UTF-8', 'windows-1252',$datos[0]['fecha'])),0,'');
$pdf->Ln($interlineado);
$pdf->Row(array('','Hora:',iconv('UTF-8', 'windows-1252',$datos[0]['hora'])),0,'');
$pdf->Ln($interlineado);
$pdf->SetWidths(array(52,130));
$pdf->SetAligns(array('L','L'));
$pdf->Row(array('
Tipo Movimiento:','
'.iconv('UTF-8', 'windows-1252',$datos[0]['idconcepto']).' '.iconv('UTF-8', 'windows-1252',$datos[0]['concepto'])),$borde,'',$alto);
$pdf->Ln($interlineado);
$pdf->Row(array('
C�digo SAE:','
'.iconv('UTF-8', 'windows-1252',$datos[0]['codigosae'])),$borde,'',$alto);
$pdf->Ln($interlineado);
$pdf->Row(array('
Descripci�n SAE:','
'.iconv('UTF-8', 'windows-1252',$datos[0]['producto'])),$borde,'',$alto);
$pdf->Ln($interlineado);
$pdf->Row(array('
Almac�n:','
'.iconv('UTF-8', 'windows-1252',$datos[0]['almacen'])),$borde,'',$alto);
$pdf->Ln($interlineado);
$pdf->Row(array('
Proveedor:','
'.iconv('UTF-8', 'windows-1252',$datos[0]['proveedor'])),$borde,'',$alto);
$pdf->Ln($interlineado);
$pdf->Row(array('
Cantidad:','
'.iconv('UTF-8', 'windows-1252',$datos[0]['cantidad']).' '.iconv('UTF-8', 'windows-1252',$datos[0]['um']).'(s)'),$borde,'',$alto);
$pdf->Ln($interlineado);
$pdf->SetWidths(array(53,37,53,37));
$pdf->Row(array('
Peso Neto:','
'.iconv('UTF-8', 'windows-1252',$datos[0]['pesoneto']).' Kg.','
Peso Bruto:','
'.iconv('UTF-8', 'windows-1252',$datos[0]['pesobruto']).' Kg.'),$borde,'',$alto);
$pdf->Ln($interlineado);
$pdf->Row(array('
Tarima:','
'.iconv('UTF-8', 'windows-1252',$datos[0]['tarima1']),'
De:','
'.iconv('UTF-8', 'windows-1252',$datos[0]['tarima2'])),$borde,'',$alto);
$pdf->Ln($interlineado);
$pdf->SetWidths(array(52,128));
$pdf->Row(array('
Documento:','
'.iconv('UTF-8', 'windows-1252',$datos[0]['documento'])),$borde,'',$alto);
$pdf->Ln(3);
$pdf->Row(array('
Ubicaci�n:','
'.iconv('UTF-8', 'windows-1252',$datos[0]['ubicacion'])),$borde,'',$alto);
$pdf->Image('../qrcodes/'.$nombreqr.'.png',75,220,70);
$pdf->Output();
?>

