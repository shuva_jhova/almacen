﻿<?php
require_once("usersFunction.php");
$app = new usersFunction();
	if(!$app->islogged()){
		echo "<script>window.top.location.href = 'logout.php';</script>";	
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="initial-scale=1.0,user-scalable=yes"/>

<link rel="stylesheet" href="../css/estilo2.css">

<link rel="stylesheet" href="../css/jquery.dataTables.min.css">
 <script language="javascript" type="text/javascript" src="../js/gateway.js"></script>
<script language="javascript" type="text/javascript" src="../js/jquery-1.12.4.js"></script>
<script language="javascript" type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
  <script language="javascript" type="text/javascript" src="../js/salidaptfabrica.js"></script>
<title></title>

</head>
<body onload="getetiquetas();">
<main>
	
	<section id="titulo">
		<center><h2>Etiquetas de Salida de Materia Prima a Fábrica</h2>
		</center>
	</section>
<div>
	<form name="f1" action="#">
		<center>
		<div id="sin2" style="display:none;">
			<div id="myDiv">
				<!--div class="txt">
					Ingresa el Nombre del Cliente a buscar<br>
					<input type="text" name="num" placeholder="Nombre del cliente" id="txtbuscar" onkeyup="doSearch()" required="required" class="TT" />
				
				</div-->
				<div class="txt" style="display:none;">
						<input type="button" name="insertar" Value="Actualizar" onClick="getclientes();" id="ok"/>
				</div>
			<div >
				<img src="../img/load.gif" class="imgload" id="imgload">
			</div>
		</div>
		</center>
	</form>
</div>
<center>
</br>
	<div class="txt" id="addne">
			<input type="button" name="insertar" Value="Agregar" onClick="agregar();" id="ok"/>
	</div>
	</br>
<div id="scro">
 <div id="tabla">
    
 <table class="tbl-qa display nowrap" style="width:100%"id="resultado">
		  <thead>
		 <tr>
		<th class="table-header" >Folio</th>
		<th class="table-header" >Fecha</th>
		<th class="table-header" >Pedido SAE</th>
		<th class="table-header" >Código SAE</th>
		<th class="table-header" >Descripción SAE</th>
                <th class="table-header" >Cantidad</th>
	        <th class="table-header" >Cliente</th>
		<th class="table-header" >Acciones</th>
		
              </tr>
 		  </thead>
		  <tbody style="height:250px;overflow:scroll">				
             	 </tbody>
              </table>
 </div>
	</br></br>
		<div id="sin" style="display:none;">
			<div class='myDiv'><div class='txt'>NO HAY ETIQUETAS REGISTRADOS EN EL SISTEMA </div></div>
		</div>
 </div>
</center>
</div>

<!--////////////////////////////////////////////////CLIENTES/////////////////////////////////////////////////////////////////////////////////-->
<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content"> 
  <div class="modal-content2">	
	<datalist id="productos"></datalist>
	<datalist id="movimientos"></datalist>
	<datalist id="almacenes"></datalist>
	<datalist id="clientes"></datalist>
	<datalist id="ums"></datalist>
	<datalist id="ubicaciones"></datalist>
    <span class="close" id="closeclientee">&times;</span>
			<section id="titulo">
        			<center></br><h2>Ingrese los Datos de la Etiqueta</h2>
					     <h5>Asegurese de que los Datos Introducidos sean Correctos</h5>
				</center>
			</section>
			</br>
	<div class="contenedor">
	<center>
				<div class="myDiv" >
					<div class="txt">
						Escane el Código<br>
						<input type="text" name="num" placeholder="..." id="txtinfocodigo" onInput="databycode()" required="required" class="TT" />	
					</div>
				</div>
			</center>

			
	<div class="myDiv" >
			<div class="txt">
				Pedido SAE<br>
				<input type="text" name="num" placeholder="Pedido SAE" id="txtpedidosae" onBlur="autocomple();" list="productos" required="required" class="TT" />	
			</div>
			<div class="txt">
				Codigo SAE<br>
				<input type="text" name="num" placeholder="Codigo SAE" id="txtcodigosae" disabled required="required" class="TT" />	
			</div>
			<div class="txt">
				Descripción SAE<br>
				<input type="text" name="num" placeholder="Descripción SAE" id="txtdescripcionsae"  disabled required="required" class="TT" />	
			</div>
			<!--div class="txt">
				Concepto y/o Movimiento<br>
				<select id="optionmovimiento" class="TT">
				</select>	
				<input type="text" name="num" placeholder="Concepto y/o Movimiento" list='movimientos' id="txtmovimiento" required="required" class="TT" />	
			</div-->
			<!--div class="txt">
				Almacen<br>
				<select id="optionalmacen" class="TT">
				</select>
				<input type="text" name="num"  placeholder="Almacén" id="txtalmacen" list="almacenes" required="required" class="TT" />	
			</div-->
			<div class="txt">
				Cliente<br>
				<input type="text" name="num"  placeholder="Cliente" id="txtcliente" disabled required="required" class="TT" />	
			</div>
			<div class="txt">
				Cantidad<br>
				<input type="text" name="num" placeholder="Cantidad" id="txtcantidad" required="required" class="TT" />	
			</div>
			<div class="txt">
				Unidad de Medida<br>
				<!--select id="optionum" class="TT">
				</select-->
				
				<input type="text" name="num" placeholder="Unidad de Medida" id="txtum"  disabled required="required" class="TT" />		
			</div>
			<div class="txt">
				Peso Neto (Kg.)<br>
				<input type="text" name="num"  placeholder="Peso Neto" id="txtpesoneto" required="required" class="TT" />	
			</div>
			<div class="txt">
				Peso Bruto (Kg.)<br>
				<input type="text" name="num" placeholder="Peso Bruto" id="txtpesobruto" required="required" class="TT" />	
			</div>
			<div class="txt">
				Tarima<br>
				<input type="text" name="num" placeholder="..." id="txttarima1" required="required" class="TT" />	
				De<br>
				<input type="text" name="num" placeholder="..." id="txttarima2" required="required" class="TT" />
			</div>
			<div class="txt">
				Número de Documento<br>
				<select id="optiondocumento" class="TT">
				</select>
				<!--input type="text" name="num" placeholder="Número de documento" id="txtdocumento" required="required" class="TT" /-->	
			</div>
			<!--div class="txt" >
				Ubicación<br>
				<select id="optionubicacion" class="TT">
				</select>
				<input type="text" name="num" placeholder="Ubicación" id="txtubicacion" list='ubicaciones' value='.' required="required" class="TT" />	
			</div-->

	</div>
			
			
			

	<center>
		<div class="txt">
			<input type="button" name="insertar" Value="Guardar" onClick="saveetiqueta();" id="ok"/>
		</div>
	
	</center>
	</div>   
  </div>
  </div>
  
</div>



<!--////////////////////////////////////////////////////////////////////////////////////EDIT CLIENT//////////////////////////////////////////////////////////////////////-->

<div id="editclient" class="modal">

  <!-- Modal content -->
  <div class="modal-content"> 
  <div class="modal-content2">	
	
    <span class="close" id="closedit">&times;</span>
			<section id="titulo">
        			<center></br><h2>Ingrese los datos a modificar del Cliente</h2>
					     <h5>Asegurese de que los datos introducidos sean correctos</h5>
				</center>
			</section>
			</br>
	<div class="contenedor">
			
	<div class="myDiv" >
		<div class="txt">
			Razon social<br>
			<input type="text" name="num" placeholder="Razon Social" id="txtrazon2" required="required" class="TT" />	
		</div>
		<div class="txt">
			Giro<br>
			<input type="text" name="num" placeholder="Giro" list="girosss" id="txtgiro2" required="required" class="TT" />	
		</div>
		<div class="txt" style="display:none;">
			Domicilio Fiscal<br>
			<textarea rows="10" cols="30" name="dir" id="txtdomicilio2" placeholder="..."></textarea> 	
		</div>
		<div class="txt" id="vendedor2" style="display:none;">
			Vendedor Asociado<br>
			<select id="optionvendedor2"></select>	
		</div>
		<div class="txt">
			Exportador<br>
			<label class="fondotxt">
				<input type='radio' name='exporta2' value='1' ">Si
				<input type='radio' name='exporta2' value='0'  checked='checked'">No
			</label>
		</div>
		<div class="txt">
			Activo<br>
			<select name="num" placeholder="Puesto" id="optionactivo2" required="required" class="TT">
				
			</select>	
		</div>
	</div>
			<!--section id="titulo">
        			<center>
					     <h5>Persona de contacto.</h5>
				</center>
			</section>
	<div class="myDiv" >
		<div class="txt">
			Nombre(s)<br>
			<input type="text" name="num" placeholder="Nombre" id="txtnombre2" required="required" class="TT" />	
		</div>
		<div class="txt">
			Primer Apellido<br>
			<input type="text" name="num" placeholder="Primer Apellido" id="txtappat2" required="required" class="TT" />	
		</div>
		<div class="txt">
			Segundo Apellido<br>
			<input type="text" name="num" placeholder="Segundo Apellido" id="txtapmat2" required="required" class="TT" />	
		</div>
		<div class="txt">
			Telefono<br>
			<input type="text" name="num" maxlength="10" placeholder="Telefono" id="txttelefono2" required="required" class="TT" />	
		</div>
		<div class="txt">
			Área<br>
			<input type="text" name="num" placeholder="Área" id="txtarea2" required="required" class="TT" />	
		</div>
		<div class="txt">
			Puesto<br>
			<input type="text" name="num" placeholder="Puesto" id="txtpuesto2" required="required" class="TT" />	
		</div>
		
	</div-->
			<section id="titulo">
        			<center>
					     <h5>Posibilidades de Venta</h5>
				</center>
			</section>
	<div class="myDiv" >
		<div class="txt">
			Etiqueta<br>
			<label class="fondotxt">
				<input type='radio' name='etiqueta2' value='1' ">Si
				<input type='radio' name='etiqueta2' value='0'  checked='checked'">No
			</label>
		</div>

		<div class="txt">
			Caja Plegadiza<br>
			<label class="fondotxt">
				<input type='radio' name='plegadiza2' value='1' ">Si
				<input type='radio' name='plegadiza2' value='0'  checked='checked'">No
			</label>
		</div>

		<div class="txt">
			Caja Single Face<br>
			<label class="fondotxt">
				<input type='radio' name='singleface2' value='1' ">Si
				<input type='radio' name='singleface2' value='0'  checked='checked'">No
			</label>
		</div>
		<div class="txt">
			Caja Plástico Corrugado<br>
			<label class="fondotxt">
				<input type='radio' name='cajaplastico2' value='1' ">Si
				<input type='radio' name='cajaplastico2' value='0'  checked='checked'">No
			</label>
		</div>
		<div class="txt">
			Displays<br>
			<label class="fondotxt">
				<input type='radio' name='display2' value='1' ">Si
				<input type='radio' name='display2' value='0'  checked='checked'">No
			</label>
		</div>


	</div>
			<section id="titulo">
        			<center>
					     <h5>Oportunidades</h5>
				</center>
			</section>
	<div class="myDiv" >

		<div class="txt">
			Corto Tiraje<br>
			<label class="fondotxt">
				<input type='radio' name='tiraje2' value='1' ">Si
				<input type='radio' name='tiraje2' value='0'  checked='checked'">No
			</label>
		</div>
		<div class="txt">
			Dato Variable.<br>
			<label class="fondotxt">
				<input type='radio' name='variable2' value='1' ">Si
				<input type='radio' name='variable2' value='0'  checked='checked'">No
			</label>
		</div>
		<div class="txt">
			Web to Print<br>
			<label class="fondotxt">
				<input type='radio' name='web2' value='1' ">Si
				<input type='radio' name='web2' value='0'  checked='checked'">No
			</label>
		</div>
		<div class="txt">
			Diseño e Ingeniería<br>
			<label class="fondotxt">
				<input type='radio' name='ingenieria2' value='1' ">Si
				<input type='radio' name='ingenieria2' value='0'  checked='checked'">No
			</label>
		</div>

		<div class="txt">
			Proyecto XMPIE<br>
			<label class="fondotxt">
				<input type='radio' name='xmpie2' value='1' ">Si
				<input type='radio' name='xmpie2' value='0'  checked='checked'">No
			</label>
		</div>

	</div>
			<section id="titulo">
        			<center>
					     <h5>Beneficios a Generar</h5>
				</center>
			</section>

	<div class="myDiv" >

		<div class="txt">
			Aumentar Ventas<br>
			<label class="fondotxt">
				<input type='radio' name='ventas2' value='1' ">Si
				<input type='radio' name='ventas2' value='0'  checked='checked'">No
			</label>
		</div>		

		<div class="txt">
			Mejorar Costos Evidentes<br>
			<label class="fondotxt">
				<input type='radio' name='costosevidentes2' value='1' ">Si
				<input type='radio' name='costosevidentes2' value='0'  checked='checked'">No
			</label>
		</div>
		<div class="txt">
			Mejorar Costos no Evidentes.<br>
			<label class="fondotxt">
				<input type='radio' name='costosnoevidentes2' value='1' ">Si
				<input type='radio' name='costosnoevidentes2' value='0'  checked='checked'">No
			</label>
		</div>
		<div class="txt" style="display:none;">
			Mejorar su participación de mercado.<br>
			<label class="fondotxt">
				<input type='radio' name='participacion2' value='1' ">Si
				<input type='radio' name='participacion2' value='0'  checked='checked'">No
			</label>
		</div>
		<div class="txt">
			Mejorar Satisfacción Clientes<br>
			<label class="fondotxt">
				<input type='radio' name='satisfaccion2' value='1' ">Si
				<input type='radio' name='satisfaccion2' value='0'  checked='checked'">No
			</label>
		</div>
		<div class="txt">
			Marcar Diferencia Competencia<br>
			<label class="fondotxt">
				<input type='radio' name='competencia2' value='1' ">Si
				<input type='radio' name='competencia2' value='0'  checked='checked'">No
			</label>
		</div>
		<div class="txt">
			Estimación de Venta Anual con Cliente<br>
			<label class="fondotxt">
				<input type='number' id='txtestimacionventa2' value='0' ">
			</label>
		</div>
	</div>
	</br>
	<div class="myDiv" >

		<div class="txt">
			¿Cúal es la estrategia que Seguirá con este Cliente?<br>
			<label class="fondotxt">
				<textarea rows="10" cols="30" name="dir" id="txtestrategia2" placeholder="..."></textarea> 	
			</label>
		</div>		
	</div>
	<div class="myDiv" style="display:none;">
		<div class="txt">
			Productos.<br>
			<textarea rows="10" cols="30" name="dir" id="txtproductos2" placeholder="..."></textarea> 	
		</div>
		<div class="txt">
			Consumidor.<br>
			<textarea rows="10" cols="30" name="dir" id="txtconsumidor2" placeholder="..."></textarea> 	
		</div>
		<div class="txt">
			Canal de Venta.<br>
			<textarea rows="10" cols="30" name="dir" id="txtcanal2" placeholder="..."></textarea> 	
		</div>
		<div class="txt">
			Competencia.<br>
			<textarea rows="10" cols="30" name="dir" id="txtcompetencia2" placeholder="..."></textarea> 	
		</div>
		<div class="txt">
			Visión y Estrategia General<br>
			<textarea rows="10" cols="30" name="dir" id="txtvision2" placeholder="..."></textarea> 	
		</div>
		<div class="txt">
			Estimacion de venta anual.<br>
			<textarea rows="10" cols="30" name="dir" id="txtestimacionventa1" placeholder="..."></textarea> 	
		</div>
		<div class="txt">
			Nececidades que el prospecto declara.<br>
			<textarea rows="10" cols="30" name="dir" id="txtnececidades2" placeholder="..."></textarea> 	
		</div>
		<div class="txt">
			Problematica que el prospecto declara.<br>
			<textarea rows="10" cols="30" name="dir" id="txtproblematica2" placeholder="..."></textarea> 	
		</div>
		<div class="txt">
			Proveedores actuales del prospecto.<br>
			<textarea rows="10" cols="30" name="dir" id="txtproveedores2" placeholder="..."></textarea> 	
		</div>
		<div class="txt">
			Espectativas del prospecto con nuestra empresa.<br>
			<textarea rows="10" cols="30" name="dir" id="txtespectativas2" placeholder="..."></textarea> 	
		</div>
						
	</div> 
			

	<center>
	<div class="txt">
	<input type="button" name="insertar" Value="Guardar" onClick="updateCliente();" id="ok"/>
	</div>
	</center>
	</div>   
  </div>
  </div>
  
</div>






</main>
</body>
</html>
