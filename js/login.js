function tipoofbrowser(){
var type="";
  // Opera 8.0+
    var isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;

    // Firefox 1.0+
    var isFirefox = typeof InstallTrigger !== 'undefined';

    // Safari 3.0+ "[object HTMLElementConstructor]" 
    var isSafari = /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || safari.pushNotification);

    // Internet Explorer 6-11
    var isIE = /*@cc_on!@*/false || !!document.documentMode;

    // Edge 20+
    var isEdge = !isIE && !!window.StyleMedia;

    // Chrome 1+
    var isChrome = !!window.chrome && !!window.chrome.webstore;

    // Blink engine detection
    var isBlink = (isChrome || isOpera) && !!window.CSS;

	if(!isChrome){
		alert("Estas usando un navegador distinto a Chrome, algunas funciones y caracteres especiales no estan disponibles en otros navegadores por lo cual el sistema  podria dejar de funcionar correctamente.");
	} 
	if(isEdge){
	//alert("Estas usando un navegador distinto a Chrome, algunas funciones y caracteres especiales no estan disponibles en otros navegadores por lo cual el sistema  podria dejar de funcionar correctamente.");
	}
detectchrome();
}
function detectchrome(){
	if(window.chrome){
		//alert("Browser is Chrome");
		//openInChrome("192.168.1.60/Cotizador");
	}
	else{
		alert("Estas usando un navegador distinto a Chrome, algunas funciones y caracteres especiales no estan disponibles en otros navegadores por lo cual el sistema  podria dejar de funcionar correctamente.");
		//openInNewTab("https://www.google.com.mx/chrome/");
		//openInChrome("192.168.1.60/Cotizador");
	}
}

function openInChrome(url) {
  
}

function openInNewTab(url) {
 // var win = window.open(url);
 // win.focus();
window.location.href=url;
}
function muestraReloj() {
			var fechaHora = new Date();
			var horas = fechaHora.getHours();
			var minutos = fechaHora.getMinutes();
			var segundos = fechaHora.getSeconds();
			if(horas < 10) { horas = '0' + horas; }
			if(minutos < 10) { minutos = '0' + minutos; }
			if(segundos < 10) { segundos = '0' + segundos; }
			document.getElementById("reloj").innerHTML = horas+':'+minutos+':'+segundos;
			}
function reloj() {

setInterval(muestraReloj, 1);
}

function login(){

	
	var user=document.getElementById("txtuser").value;
	var pass=document.getElementById("txtpassword").value;
	
		//localStorage.setItem('estacion',estacion);
		REQUEST('usersFunction', 'login', [user,pass], onLogin, onerror);
}


function onerror(errObject)
{
		alert(errObject["Message"] + ":\n" + errObject["Detail"]);
}


function onLogin(dataResult)
{
	if(dataResult.length>0)
	{	
		if(dataResult[0]['usuario'].localeCompare("en sesion")!=0){
			localStorage.setItem('id',dataResult[0]['idusuario']);
			localStorage.setItem('perfil',dataResult[0]['idperfil']);
			localStorage.setItem('operador',dataResult[0]['operador']);
			localStorage.setItem('nombre',dataResult[0]['nombre']);
			localStorage.setItem('usuario',dataResult[0]['usuario']);
			localStorage.setItem('vendedor',dataResult[0]['vendedor']);
			document.location.href = 'php/inicio.php';
		}
		else{
			alert('Este usuario tiene una sesi\u00F3n activa, por el momento no puede volver a ingresar. Intente nuevamente m\u00E1s tarde.');
		}
	}else{
		alert('Nombre de usuario y/o clave de acceso incorrectos. Intente nuevamente.');
	
	}
}

function getestaciones(){
	
	REQUEST('appControl', 'getestacionesactivas', [] , onEstaciones, onerror);
}

function onEstaciones(dataResult){
	if(dataResult.length>0){
		var op=document.getElementById("optionestacion");		
		for (var a = 0; a<dataResult.length; a++)
		{	
			var option = document.createElement("option");
	    		option.value=dataResult[a]['id'];
    			option.text =dataResult[a]['nombre'];
			op.appendChild(option);
		}
	}
}
  function async_alert(message) {
     $("body").append("<div class='alert-box'>\
           <div class='alert-surface'>\
              <h2>Alert</h2></header>\
              <p>" + message + "</p>\
              <button type='button' class='alert-remove'>OK</button>\
           </div>\
           <div class='alert-backdrop'></div>\
        </div>");
     $(".alert-remove").off("click").on("click", function() { $(".alert-box").remove(); });
  }