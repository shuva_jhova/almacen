var str2;
var editmodulos;
var modulosdata;
var modulosdata2;
var inedition=false;
function showEdit(editableObj) {
			editableObj.style.backgroundColor="#e3bfc4";

		}
function edita(editableObj) {
			editableObj.style.backgroundColor="#FFCC99";
		}

function onerror(errObject)
{
		alert(errObject["Message"] + ":\n" + errObject["Detail"]);
}


function saverecord(str)
{

	var nombre = document.getElementById('nombre'+str).innerText;
	var numero = document.getElementById('numero'+str).innerText;
	
	var activo = document.getElementById('optionactivo'+str).value;
	
 	if(nombre.length == 0)
	{
		alert("El nombre de la ubicaci\u00E3n es inv\u00E1lido");
	}
	else
	{
		REQUEST('appControlalmacen', 'updatemovimiento', [str,nombre,numero,activo], onUpdatemovimiento, onerror);
	}
}

function onUpdatemovimiento(dataResult)
{
	if(dataResult.length>0){
		inedition=false;
		if(dataResult[0]['exito'].localeCompare("si")==0)
		{		
			alert("\u00C9xito al modificar los datos");
			var iframe = window.parent.document.getElementById("iframe");
			iframe.contentWindow.location.reload(true);
		}
		else if(dataResult[0]['exito'].localeCompare("sin")==0)
		{
			alert("Los datos no han sido encontrados");
		}
	}else
	{
		alert("Error al modificar los datos. Intente nuevamente");
	}
    	
	
}


function agregar(){
// Get the modal
var modal = document.getElementById('myModal');


// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];
  modal.style.display = "block";

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
	}


}

function savemovimiento(){
		

	var nombre=document.getElementById('txtnombre').value;
	var numero=document.getElementById('txtnumero').value;
	
	
	if(nombre.length==0){
		alert("El nombre y/o coordenadas de la ubicaci\u00F3n son inv\u00E1lidos");
	}else{
		REQUEST('appControlalmacen', 'savemovimiento', [nombre,numero] , onsavemovimiento, onerror);
	}

}
function onsavemovimiento(dataResult){
	if(dataResult[0]['exito'].localeCompare("si")==0)
	{
		alert("\u00C9xito al Guardar Datos("+dataResult[0]['folio']+")");
		var iframe = window.parent.document.getElementById("iframe");
		iframe.contentWindow.location.reload(true);
	}
	else if(dataResult[0]['exito'].localeCompare("ya")==0)
	{
		alert("Los datos ya ha sido dados de alta");
	}
	else if(dataResult[0]['exito'].localeCompare("no")==0)
	{	
		alert("Error al Guardar Datos. Intente nuevamente");
	}

}

function getmovimientos(){
	document.getElementById("imgload").style.display="block";
	REQUEST('appControlalmacen', 'getmovimientos', [] , onmovimientos, onerror);
}

function onmovimientos(dataResult){
	inedition=false;
	document.getElementById("imgload").style.display="none";
	myDeleteFunction() ;
		if(dataResult.length>0){
			

		for (var i = 0; i<dataResult.length; i++)
		{		
			var tableRef = document.getElementById('resultado').getElementsByTagName('tbody')[0];
			// Insert a row in the table at the last row
			var newRow   = tableRef.insertRow(tableRef.rows.length);
			newRow.className = "table-row";
			newRow.id=dataResult[i]["id"];
			// Insert a cell in the row at index 0
			var newCell  = newRow.insertCell(0);
			newCell.id="numero"+dataResult[i]["id"];
			//newCell.contentEditable = "true";
			//newCell.addEventListener('click', showEdit.bind(null,newCell));
			// Append a text node to the cell
			var newText  = document.createTextNode(dataResult[i]["numero"]);
			newCell.appendChild(newText);

			 newCell  = newRow.insertCell(1);
			newCell.id="nombre"+dataResult[i]["id"];
			//newCell.contentEditable = "true";	
			// Append a text node to the cell
			 newText  = document.createTextNode(dataResult[i]["nombre"]);
			newCell.appendChild(newText);		
			

			newCell  = newRow.insertCell(2);
			newCell.id="activo"+dataResult[i]["id"];
			//newCell.contentEditable = "true";
			// Append a text node to the cell
			var activo="";
			if(dataResult[i]["activo"]>0){
				activo="Si";
			}else{
				activo="No";
			}
			newText  = document.createTextNode(activo);
			newCell.appendChild(newText);


			newCell  = newRow.insertCell(3);
			var img = document.createElement('img');
   			img.src = "../img/editar.png";
			img.id="imgeditar"+dataResult[i]["id"];
			img.addEventListener('click', edit.bind(null,dataResult[i]["id"]));
			newCell.appendChild(img);
			var img3 = document.createElement('img');
   			 img3.src = "../img/save.png";
			img3.id="imgsave"+dataResult[i]["id"];
			img3.style.display="none";
			img3.addEventListener('click', saverecord.bind(null,dataResult[i]["id"]));
			newCell.appendChild(img3);
			var img4 = document.createElement('img');
   			img4.src = "../img/cancel.png";
			img4.id="imgcancel"+dataResult[i]["id"];
			img4.style.display="none";
			img4.addEventListener('click', cancela.bind(null,dataResult[i]["id"]));
			newCell.appendChild(img4);

			newCell  = newRow.insertCell(4);
			var img2 = document.createElement('img');
   			img2.src = "../img/delete.png";
			img2.addEventListener('click', deleterecord.bind(null,dataResult[i]["id"]));
			newCell.appendChild(img2);

			//newRow.addEventListener('click', navigateToController.bind(null, dataResult[i]["usuario"]));
		}
		
		document.getElementById("sin").style.display="none";
		document.getElementById("tabla").style.display="block";
		document.getElementById("sin2").style.display="block";
		pagination('#resultado');

	}else{
		document.getElementById("sin").style.display="block";
		document.getElementById("tabla").style.display="none";
		document.getElementById("sin2").style.display="none";
	
	}	

}

function cancela(str){

	var si=confirm("Aunque los cambios hallan sido realizados no se guardar\u00E1n\n\u00BFSeguro que desea cancelar la modificaci\u00F3n sin guardar?");
	if(si){ 
		/*var row=document.getElementById(str);
		row.className = "table-row";
		var cells = row.getElementsByTagName('td')
		for(var a=0;a<cells.length-2;a++){
				cells[a].contentEditable = "false";
				cells[a].style.backgroundColor="mediumseagreen";
				cells[a].addEventListener('focus', null);
		}

		var activo=document.getElementById('optionactivo'+str);
		//cells[3].innerHTML=sexo;
		cells[4].innerText=activo.options[activo.selectedIndex].text;;
	
		var img1=document.getElementById('imgeditar'+str);
		var img2=document.getElementById('imgsave'+str);
		var img3=document.getElementById('imgcancel'+str);
		img1.style.display="block";
		img2.style.display="none";
		img3.style.display="none";*/
		var iframe = window.parent.document.getElementById("iframe");
		iframe.contentWindow.location.reload(true);
	}
}
function pagination(id){
		$(document).ready(function() {
   			table= $(id).DataTable( {
				"destroy":true,
      				 "scrollY": 500,
       				 "scrollX": true
    			} );
			
		} );
}


function edit(str){
if(inedition){
		return;
	}
	inedition=true;
	str2=str;
	var row=document.getElementById(str);
	row.className = "table-row2";
	var cells = row.getElementsByTagName('td')
	for(var a=0;a<cells.length-3;a++){
		cells[a].style.backgroundColor="#9adbcf";
		
		cells[a].contentEditable = "true";
		cells[a].addEventListener('focus', showEdit.bind(null,cells[a]));
		
	}
	
	cells[0].setAttribute('tabindex', 10000);    // or other number
	cells[0].focus();
	var img1=document.getElementById('imgeditar'+str);
	var img2=document.getElementById('imgsave'+str);
	var img3=document.getElementById('imgcancel'+str);

	img1.style.display="none";
	img2.style.display="block";
	img3.style.display="block";

	
	var activo=cells[2].innerText;
	cells[2].contentEditable="false";
	if(activo.localeCompare("Si")==0){
		var selectList = document.createElement("select");
   		selectList.id="optionactivo"+str;
 		var option = document.createElement("option");
    		 option.value="1"
    		 option.text = "Si"
    		 selectList.appendChild(option);
 		var option = document.createElement("option");
    		 option.value="0"
    		 option.text = "No"
    		 selectList.appendChild(option);
		
		cells[2].innerHTML="";
		cells[2].innerText="";
		cells[2].appendChild(selectList);
	}else if(activo.localeCompare("No")==0){
		
		var selectList = document.createElement("select");
   		selectList.id="optionactivo"+str;
		var option = document.createElement("option");
    		 option.value="0"
    		 option.text = "No"
    		 selectList.appendChild(option);
 		var option = document.createElement("option");
    		 option.value="1"
    		 option.text = "Si"
    		 selectList.appendChild(option);
 		  		
		cells[2].innerHTML="";
		cells[2].innerText="";
		cells[2].appendChild(selectList);
	}


	
}



function deleterecord(str){
	var res=confirm("\u00BFSeguro que desea eliminar este registro?");

	if(res){
		REQUEST('appControlalmacen', 'deletemovimiento', [str] , onMovimientodelete, onerror);
	}
}

function onMovimientodelete(dataResult){
	if(dataResult[0]['exito'].localeCompare("si")==0)
	{	
		alert("\u00C9xito al eliminar los datos");
		var iframe = window.parent.document.getElementById("iframe");
			iframe.contentWindow.location.reload(true);
			
	}
	else if(dataResult[0]['exito'].localeCompare("sin")==0)
	{
		alert("Los datos no han sido dado encontrados");
	}else
	{
		alert("Error al eliminar los datos. Intente nuevamente");
	}	
}


function myDeleteFunction() {
	var tableRef = document.getElementById('resultado');
	//var rowCount = tableRef.rows.length;
	
	var row = document.getElementsByTagName('tbody')[0];
	var rowCount = row.rows.length;

		for(var a=0;a<rowCount;a++)
		{

			//tableRef.deleteRow(1);
			row.deleteRow(0);
		}
    
}

function getval(grupo){
	var selec;

	for(var i = 0; i < grupo.length; i++) {
 	  if(grupo[i].checked)
    	   selec = grupo[i].value;
 	}
return selec;
}

function doSearch()
		{
			//var tableReg = document.getElementById('resultado');
			var tableReg = document.getElementsByTagName('tbody')[0];

			var searchText = document.getElementById('txtbuscar').value.toLowerCase();
			var cellsOfRow="";
			var found=false;
			var compareWith="";
 
			// Recorremos todas las filas con contenido de la tabla
			for (var i = 0; i < tableReg.rows.length; i++)
			{
				cellsOfRow = tableReg.rows[i].getElementsByTagName('td');
				found = false;
				// Recorremos todas las celdas
				for (var j = 0; j < cellsOfRow.length && !found; j++)
				{
					compareWith = cellsOfRow[j].innerHTML.toLowerCase();
					// Buscamos el texto en el contenido de la celda
					if (searchText.length == 0 || (compareWith.indexOf(searchText) > -1))
					{
						found = true;
					}
				}
				if(found)
				{
					tableReg.rows[i].style.display = '';
				} else {
					// si no ha encontrado ninguna coincidencia, esconde la
					// fila de la tabla
					tableReg.rows[i].style.display = 'none';
				}
			}
		}
function isNumber( input ) {
    return !isNaN( input );
}
